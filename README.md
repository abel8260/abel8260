
# Abel Barbosa (DEV CLASS A+)          

# Motivation:

- ` Its not about winning or lose. Its about be here. Right here and right now.` 


***

> I use:

![imagem](https://img.icons8.com/color/48/000000/html-5--v1.png)
![imagem](https://img.icons8.com/color/48/000000/css3.png)
![imagem](https://img.icons8.com/color/48/000000/javascript.png)
![imagem](https://img.icons8.com/ios-filled/50/000000/github-2.png)

 ***
 
 # about me :
 
A  complete fan of traditional 90's  and 2000's sites. My  sites is that side ... so you welcome here be proud of yourself because you are awesome <3           

***

 > DOC:
 
![imagem](https://img.shields.io/badge/doc-eap%20-blue)
![imagem](https://img.shields.io/badge/doc-itil%20-blue)
![imagem](https://img.shields.io/badge/doc-kpi%20-blue)
![imagem](https://img.shields.io/badge/doc-pdca%20-blue)
![imagem](https://img.shields.io/badge/doc-pmbok%20-blue)
![imagem](https://img.shields.io/badge/doc-google%20-orange) 
![imagem](https://img.shields.io/badge/doc-semrush%20-orange) 
![imagem](https://img.shields.io/badge/doc-sla%20-orange) 
![imagem](https://img.shields.io/badge/doc-smart15%20-orange) 
![imagem](https://img.shields.io/badge/doc-sow%20-orange) 

***

# :speech_balloon: About my class : 
***
![image](https://github-readme-stats.vercel.app/api?username=abel8260)
***


# :envelope: Newsletter:

***

- Name: crafting  web and paper   
- Site: (https://crafting-web-paper.herokuapp.com/src/home/index.php) :star: :star: :star: :star: :star:
- A site about the news in the 2 ORG's that i admin.


***

- Name: group pacific south;  
- Site: (https://grupo-fs-pacificsouth.herokuapp.com) :star: :star: :star: :star: :star:
- A learning programming site. 

***



# :sparkles:  Purpose: 





***
- Name: group pacific south;  
- Site: (https://www.facebook.com/grupofspacificsouth) :star: :star: :star: :star: :star:
- A learning programming site.           
***

# :mortar_board: Study goals:
***
- CS50’s Introduction to Game Development  :star: :star: :star: :star: :star:
- CS50x  :star: :star: :star: :star: :star:
- CS193p course, Developing Applications for iOS  :star: :star: :star: :star: :star:
***

***
- Name: Portfolio of studies/ Nerdtubo
- Site: https://abel8260.github.io/nerdtubo/) :star: :star: :star: :star: :star:
- A site about victories in the studies.            
*** 


# :blue_book:  sideprojects:


***
- Name: medi-game 
- Site: (https://medi-game.herokuapp.com/) :star: :star: :star: :star: :star:
- Repo: (https://github.com/abel8260/medi-game) :star: :star: :star: :star: :star:
- A  game about meditation, to more info read the documentation on the repo here in the github.   
- Progresso:   ![](https://us-central1-progress-markdown.cloudfunctions.net/progress/75)       
***

***
- Name: [APP]loy.d 
- Site: (https://apployd.herokuapp.com/) :star: :star: :star: :star: :star:
- My own appstore with apps and awesome things.            
***

***
- Name: portfolio of abel   
- Site: (https://portfolioebol.herokuapp.com/) :star: :star: :star: :star: :star:
- My own portfolio of softwares .            
***

***
- Name: APP magista      
- Site: (https://magistaapp.herokuapp.com/) :star: :star: :star: :star: :star:
- A library of memes to you play with your friends.            
***



***
- Name: kop papercraft    
- Site: (https://koppapercraft.herokuapp.com/) :star: :star: :star: :star: :star:
- This is the site about my creation on my hobby.            
***




***
- Name: Sigdu kingdom  
- Site: (https://sigdu-kingdom.herokuapp.com/) :star: :star: :star: :star: :star:
- TThis isa game Soulslike and give so much fun to players.            
***

***
- Name: Resig - game engine  
- Site: (https://resig-home.herokuapp.com/) :star: :star: :star: :star: :star:
- This is a game engine for text-adventures games used in sigdu-kingdom.            
***

***
- Name: Souls like series - Proect
- Site: (https://projeto-soulslikeseriejogo.herokuapp.com/) :star: :star: :star: :star: :star:
- This is a portal to sigdu kingdom. There are  infos  and extra content.                
***

# Refs:
- (https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
- (https://www.markdownguide.org/tools/trello/)
- (http://cursos.leg.ufpr.br/prr/capMarkdown.html)
- (https://gist.github.com/rxaviers/7360908)

# :page_facing_up: Good note:
- Goals only have value if exists all of it in the documentation.    
